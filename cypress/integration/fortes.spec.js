describe('analista de teste', () => {

    beforeEach('', () => { 
        cy.visit('https://qa-test.avenuecode.com/tasks')
        cy.get('#user_email').clear().type('patriciapereira@fortestecnologia.com.br')
        cy.get('#user_password').clear().type('juan0230@')
        cy.get('#user_remember_me').should('not.be.checked').check()
        cy.get('.btn').click()
    })

    afterEach('',() => {
        cy.get('.navbar-right > :nth-child(2) > a').click()
    })

    it('Saudação ao usuário', () => {
    
        //a saudação está diferente do esperado
        cy.contains("Patricia Pereira's ToDo List").should('be.visible')
        
    })

    it('Inclusao de tarefa pressionando enter', () => {

        //tarefa com apenas 2 caracteres - utilizando o ENTER .
        cy.get('#new_task').should('be.empty').clear().type('UM', '{enter}') 
        
    })

    it('inclusao de tarefa usando click ', () => {   
        cy.get('#new_task').should('be.empty').clear().type('Oi') 
        cy.get('.input-group-addon').click()

        //Erro de inclusão da tarefa vazia que acontece somente se houver alguma tarefa na lista
        cy.get('#new_task').clear()
        cy.get('.input-group-addon').click()
        cy.get(':nth-child(1) > .task_body > .ng-scope').type('tarefa para amanhã')
        cy.get('.editable-buttons > .btn-primary').click()

    })
    
    it('Inclusao de tarefa - validando 250 caracteres', () => {

        cy.get('#new_task').clear()
        .type('Leitura do salmo 51: Mas tu queres a sinceridade do coração e no íntimo me ensinas a sabedoria. Purifica-me com o hissopo e ficarei puro; lava-me e ficarei mais branco que a neve. Faze-me ouvir alegria e júbilo, exultem os ossos que tu quebraste. //')
        cy.get('.input-group-addon').click()

    })

    it('validar tarefa vazia', () => {

        //inclusão de uma tarefa vazia
        cy.get('#new_task').clear()
        cy.get('.input-group-addon').click()
    })

    it('Inclusão de subtarefas', () => {
        
        //inclusão da tarefa
        cy.get('#new_task').should('be.empty').clear().type('Cadastro de Empregados') 
        cy.get('.input-group-addon').click() 

        //inclusao de subtarefa da tarefa cad de empregados
        cy.get(':nth-child(1) > :nth-child(4) > .btn').click()
        cy.get('.modal-title').should('not.have.text','Editing Task 84504 Cadastro de Empregados')
        cy.get('#new_sub_task').clear().type('Validar dados admissionais')
        cy.get('#dueDate').clear().type('05/03/2021')
        cy.get('#add-subtask').click()
        
        //inclusão de subtarefa sem preenchimento de data
        cy.get('#new_sub_task').clear().type('Emitir relatorios admissionais')
        cy.get('#dueDate').clear()
        cy.get('#add-subtask').click()

        //inclusão de subtarefa com descrição superior a 250 caracteres.
        cy.get('#new_sub_task').clear().type('Leitura do salmo 51: Mas tu queres a sinceridade do coração e no íntimo me ensinas a sabedoria. Purifica-me com o hissopo e ficarei puro; lava-me e ficarei mais branco que a neve. Faze-me ouvir alegria e júbilo, exultem os ossos que tu quebraste. //')
        cy.get('#dueDate').clear().type('05/03/2021')
        cy.get('#add-subtask').click()
        cy.get('.modal-footer > .btn').click()

    })

})